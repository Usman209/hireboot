const limit = 5;

module.exports = paginate = (Model, page) => {
  return Model.findAndCountAll({
    offset: (page - 1) * limit,
    limit: limit,
  })
    .then((result) => {
      result["limit"] = limit;
      return result;
    })
    .catch((error) => {
      return error.message;
    });
};
